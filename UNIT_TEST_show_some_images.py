import os
from typing import List
import numpy as np
from config import CELEBA_DATA_DIR
from PIL import Image
from typing import Tuple, Dict
import matplotlib.pyplot as plt
import random
from common.logger import SingletonLogger


SingletonLogger.set_logger_name(name="Show some images")


def open_image_and_convert_to_numpy(file_name: str) -> np.ndarray:
    image = Image.open(file_name)
    image: np.ndarray = np.asarray(image)
    return image


def show_some_images(file_names: List[str], ax_size: int = 50, num_grids: Tuple[int, int] = (3, 5)):
    font_dict: Dict = {"family": "serif", "color": "darkred", "weight": "bold", "size": 14}
    fig = plt.figure(figsize=(num_grids[0]*ax_size, num_grids[1]*ax_size))
    file_names: List[str] = random.sample(file_names, k=num_grids[0]*num_grids[1])
    for index, file_name in enumerate(file_names):
        image: np.ndarray = open_image_and_convert_to_numpy(file_name=file_name)
        ax = fig.add_subplot(num_grids[0], num_grids[1], index+1)
        ax.set_title(f"Image {index+1}", fontdict=font_dict)
        ax.imshow(image)
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    fig.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.4)
    plt.show()


FILE_NAMES: List[str] = os.listdir(f"{CELEBA_DATA_DIR}")
FILE_NAMES: List[str] = list(filter(lambda x: x.endswith('.jpg'), FILE_NAMES))
FILE_NAMES: List[str] = list(map(lambda x: f"{CELEBA_DATA_DIR}/{x}", FILE_NAMES))
show_some_images(file_names=FILE_NAMES)



