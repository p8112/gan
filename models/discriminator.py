import torch.nn as nn
from torch import Tensor
import torch


class Discriminator(nn.Module):
    def __init__(self):
        super(Discriminator, self).__init__()
        self.core = nn.Sequential(
            nn.Conv2d(in_channels=3, out_channels=64, kernel_size=4, stride=2, padding=1, bias=False),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),    # 64, 112, 112

            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(num_features=64),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),  # 64, 56, 56

            nn.Conv2d(in_channels=64, out_channels=128, kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(num_features=128),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),  # 128, 28, 28

            nn.Conv2d(in_channels=128, out_channels=128, kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(num_features=128),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),  # 128, 14, 14

            nn.Conv2d(in_channels=128, out_channels=256, kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(num_features=256),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),  # 256, 7, 7

            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=2, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=256),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),  # 256, 8, 8

            nn.Conv2d(in_channels=256, out_channels=512, kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(num_features=512),
            nn.LeakyReLU(negative_slope=0.2, inplace=True),  # 512, 4, 4

            nn.Conv2d(in_channels=512, out_channels=1, kernel_size=4, stride=1, padding=0, bias=False),   # 1, 1, 1
        )
        self.apply(self.init_params)

    def init_params(self, module: nn.Module):
        if isinstance(module, nn.Conv2d):
            nn.init.normal_(module.weight.data, mean=0.0, std=0.02)
        elif isinstance(module, nn.BatchNorm2d):
            nn.init.normal_(module.weight.data, mean=1.0, std=0.02)
            nn.init.constant_(module.bias.data, val=0.0)

    def forward(self, x: Tensor) -> Tensor:
        """

        :param x: batch_size, 3, image_size, image_size
        :return: batch_size
        """
        batch_size: int = x.shape[0]
        x = self.core(x)    # batch_size, 1, 1, 1
        x = x.reshape((batch_size,))    # batch_size
        return x
