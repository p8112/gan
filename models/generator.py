import torch.nn as nn
from torch import Tensor


class Generator(nn.Module):
    def __init__(self, in_features: int):
        super(Generator, self).__init__()
        self.core = nn.Sequential(
            nn.ConvTranspose2d(in_channels=in_features, out_channels=512,
                               kernel_size=4, stride=1, padding=0, bias=False),
            nn.BatchNorm2d(num_features=512),
            nn.ReLU(inplace=True),    # 512, 4, 4

            nn.ConvTranspose2d(in_channels=512, out_channels=512,
                               kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(num_features=512),
            nn.ReLU(inplace=True),   # 512, 8, 8

            nn.ConvTranspose2d(in_channels=512, out_channels=256,
                               kernel_size=2, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=256),
            nn.ReLU(inplace=True),    # 256, 7, 7

            nn.ConvTranspose2d(in_channels=256, out_channels=256,
                               kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(num_features=256),
            nn.ReLU(inplace=True),    # 256, 14, 14

            nn.ConvTranspose2d(in_channels=256, out_channels=128,
                               kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(num_features=128),
            nn.ReLU(inplace=True),  # 128, 28, 28

            nn.ConvTranspose2d(in_channels=128, out_channels=128,
                               kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(num_features=128),
            nn.ReLU(inplace=True),  # 128, 56, 56

            nn.ConvTranspose2d(in_channels=128, out_channels=64,
                               kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(num_features=64),
            nn.ReLU(inplace=True),  # 128, 112, 112

            nn.ConvTranspose2d(in_channels=64, out_channels=3,
                               kernel_size=4, stride=2, padding=1, bias=False),
            nn.Tanh()    # 3, 224, 224
        )
        self.apply(self.init_params)

    def init_params(self, module: nn.Module):
        if isinstance(module, nn.ConvTranspose2d):
            nn.init.normal_(module.weight.data, mean=0.0, std=0.02)
        elif isinstance(module, nn.BatchNorm2d):
            nn.init.normal_(module.weight.data, mean=1.0, std=0.02)
            nn.init.constant_(module.bias.data, val=0.0)

    def forward(self, x: Tensor) -> Tensor:
        """

        :param x: batch_size, in_features
        :return: batch_size, 3, image_size, image_size
        """
        x = x.unsqueeze(dim=2).unsqueeze(dim=3)   # batch_size, in_features, 1, 1
        x = self.core(x)
        return x
