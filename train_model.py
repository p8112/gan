import os
from typing import List, Dict, Tuple

import matplotlib
import numpy as np
from common.utils import JsonWriteObjectToLocalPatient, LocalFileHandlerUtils
from config import (
    CELEBA_DATA_DIR, IMAGE_SIZE, BATCH_SIZE, LATENT_DIM,
    DEVICE_NAME, DROP_OUT, LEARNING_RATE, NUM_STEP_UPDATE_GENERATOR, NUM_EPOCHS, NUM_STEP_SAVE_IMAGE
)
from torch.utils.data import DataLoader
from datasets import ImageDataset
from common.logger import SingletonLogger
from models import Generator, Discriminator
import torch
from torch.optim import Adam
from torch.nn import BCEWithLogitsLoss
from torch import Tensor
from datetime import datetime
from tqdm import tqdm
import matplotlib.pyplot as plt
import gc


matplotlib.use("Agg")


SingletonLogger.set_logger_name(name="Train model")

LOG_DIR: str = f"log_dir/{datetime.now().strftime('%Y_%m_%d %H_%M_%S')}"

DEVICE = torch.device(DEVICE_NAME) if torch.cuda.is_available() else torch.device("cpu")

FILE_NAMES: List[str] = os.listdir(f"{CELEBA_DATA_DIR}")
FILE_NAMES: List[str] = list(filter(lambda x: x.endswith('.jpg'), FILE_NAMES))
FILE_NAMES: List[str] = list(map(lambda x: f"{CELEBA_DATA_DIR}/{x}", FILE_NAMES))

DATASET = ImageDataset(file_names=FILE_NAMES, image_size=IMAGE_SIZE)
DATALOADER = DataLoader(DATASET, batch_size=BATCH_SIZE, shuffle=True)

GENERATOR = Generator(in_features=LATENT_DIM)
GENERATOR = GENERATOR.to(DEVICE)
print("GENERATOR ARCHITECTURE")
print(GENERATOR)

DISCRIMINATOR = Discriminator()
DISCRIMINATOR = DISCRIMINATOR.to(DEVICE)
print("DISCRIMINATOR ARCHITECTURE")
print(DISCRIMINATOR)

LOSSER = BCEWithLogitsLoss(reduction="mean").to(DEVICE)

GENERATOR_OPTIMIZER = Adam(GENERATOR.parameters(), lr=LEARNING_RATE, betas=(0.5, 0.999))
DISCRIMINATOR_OPTIMIZER = Adam(DISCRIMINATOR.parameters(), lr=LEARNING_RATE, betas=(0.5, 0.999))

CONFIG: Dict = {
    "device": str(DEVICE),
    "image_size": IMAGE_SIZE,
    "batch_size": BATCH_SIZE,
    "latent_dim": LATENT_DIM,
    "drop_out": DROP_OUT,
    "learning_rate": LEARNING_RATE,
    "num_step_update_generator": NUM_STEP_UPDATE_GENERATOR,
    "num_epochs": NUM_EPOCHS
}

JsonWriteObjectToLocalPatient().write(x=CONFIG, file_name=f"{LOG_DIR}/config.json")


def train_step(true_image: Tensor):
    GENERATOR.train()
    DISCRIMINATOR.train()

    batch_size: int = true_image.shape[0]
    positive_label = torch.ones(size=(batch_size,)).to(DEVICE)
    negative_label = torch.zeros(size=(batch_size,)).to(DEVICE)
    true_image = true_image.to(DEVICE)
    rand_z = torch.randn(size=(batch_size, LATENT_DIM)).to(DEVICE)
    pseudo_image = GENERATOR(x=rand_z)

    true_predict = DISCRIMINATOR(x=true_image)
    pseudo_image_detach = pseudo_image.detach()
    pseudo_predict = DISCRIMINATOR(x=pseudo_image_detach)
    discriminator_loss = LOSSER(true_predict, positive_label) + LOSSER(pseudo_predict, negative_label)

    DISCRIMINATOR.zero_grad()
    DISCRIMINATOR_OPTIMIZER.zero_grad()
    discriminator_loss.backward()
    DISCRIMINATOR_OPTIMIZER.step()

    del true_image, true_predict, pseudo_predict, negative_label, pseudo_image_detach

    pseudo_predict = DISCRIMINATOR(x=pseudo_image)
    generator_loss = LOSSER(pseudo_predict, positive_label)

    GENERATOR.zero_grad()
    DISCRIMINATOR.zero_grad()
    GENERATOR_OPTIMIZER.zero_grad()
    generator_loss.backward()
    GENERATOR_OPTIMIZER.step()

    with open(f"{LOG_DIR}/train_sum_up.txt", mode="a") as file_obj:
        file_obj.write(f"Generator Loss: {generator_loss.item():.3f}  Discriminator Loss: {discriminator_loss.item():.3f}  \n")

    del rand_z, pseudo_image, pseudo_predict, positive_label
    del discriminator_loss, generator_loss
    torch.cuda.empty_cache()


LocalFileHandlerUtils.check_and_make_directory(directory=f"{LOG_DIR}/images")


def save_generator_output(global_step: int, ax_size: int = 50, num_grids: Tuple[int, int] = (3, 3)):
    with torch.no_grad():
        GENERATOR.eval()
        pseudo_image: Tensor = GENERATOR(x=torch.randn(size=(num_grids[0]*num_grids[1], LATENT_DIM)).to(DEVICE))
        pseudo_image_numpy: np.ndarray = pseudo_image.permute(0, 2, 3, 1).cpu().detach().numpy()    # batch_size, height, width, num_channels

    fig = plt.figure(figsize=(num_grids[0] * ax_size, num_grids[1] * ax_size))
    for index in range(pseudo_image_numpy.shape[0]):
        image: np.ndarray = pseudo_image_numpy[index]
        ax = fig.add_subplot(num_grids[0], num_grids[1], index + 1)
        ax.imshow(image)
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    plt.savefig(f"{LOG_DIR}/images/step_{global_step:06d}.jpg")
    plt.close(fig)
    del pseudo_image, pseudo_image_numpy
    torch.cuda.empty_cache()
    gc.collect()


GLOBAL_STEP: int = 0
STEP_WITHOUT_UPDATE_GENERATOR: int = 0
for EPOCH in range(NUM_EPOCHS):
    PROGRESS_BAR = tqdm(DATALOADER, desc=f"Training epoch {EPOCH+1}...")
    for IMAGE in PROGRESS_BAR:
        train_step(true_image=IMAGE)
        del IMAGE
        GLOBAL_STEP += 1
        if GLOBAL_STEP % NUM_STEP_SAVE_IMAGE == 0:
            save_generator_output(global_step=GLOBAL_STEP)
    PROGRESS_BAR.close()
    torch.save(GENERATOR.state_dict(), f"{LOG_DIR}/generator.pt")
    torch.save(DISCRIMINATOR.state_dict(), f"{LOG_DIR}/discriminator.pt")
save_generator_output(global_step=GLOBAL_STEP)
