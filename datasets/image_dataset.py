from torch.utils.data import Dataset
from typing import List
import torch
from torch import Tensor
from PIL import Image
import torchvision.transforms as torch_transform


class ImageDataset(Dataset):
    def __init__(self, file_names: List[str], image_size: int):
        super(ImageDataset, self).__init__()
        self.file_names: List[str] = file_names
        self.transformer = torch_transform.Compose([
            torch_transform.Resize(image_size),
            torch_transform.CenterCrop(image_size),
            torch_transform.ToTensor(),
            torch_transform.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5))
        ])

    def __len__(self) -> int:
        return len(self.file_names)

    def __getitem__(self, item: int) -> Tensor:
        file_name: str = self.file_names[item]
        image = Image.open(file_name)
        image: Tensor = self.transformer(image)
        return image
